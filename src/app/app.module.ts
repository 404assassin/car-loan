import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
// Components
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
import { AppComponent } from './app.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { NewAccountComponent } from './components/pages/new-account/new-account.component';
import { DisqualificationComponent } from './components/pages/disqualification/disqualification.component';
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
// Materials
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorStateMatcher } from '@angular/material/core';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
    declarations: [
        AppComponent,
        LandingComponent,
        NewAccountComponent,
        DisqualificationComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        MatButtonModule,
        HttpClientModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        ReactiveFormsModule,
    ],
    providers: [
        CurrencyPipe,
        {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
