import { FormControl, FormGroup, FormGroupDirective, NgForm, ValidationErrors, ValidatorFn } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export const ConfirmMatchValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('password');
    const passwordConfirm = control.get('passwordConfirm');

    // console.log(
    //     '\n:::::::::::::::::::::::::::::::::::::: ConfirmMatchValidator Validator :::::::::::::::::::::::::::::::::::::::::::::::::::',
    //     '\n::this::', this,
    //     '\n::control::', control,
    //     '\n::password::', password,
    //     '\n::password::', password.value,
    //     '\n::passwordConfirm::', passwordConfirm,
    //     '\n::password && passwordConfirm && password !== passwordConfirm::', password && passwordConfirm && password.value !== passwordConfirm.value,
    //     '\n::password && passwordConfirm && password === passwordConfirm::', password && passwordConfirm && password.value === passwordConfirm.value,
    //     '\n::control.get(controlName)::', control.get('password'),
    //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
    // );
    return password && passwordConfirm && password.value !== passwordConfirm.value ? {'confirmMatchValidator': true} : null;
};

/**
 * Custom ErrorStateMatcher which returns true (error exists) when the parent form group is invalid and the control has been touched
 */
export class ConfirmValidParentMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        return control.parent.invalid && control.touched;
    }
}
