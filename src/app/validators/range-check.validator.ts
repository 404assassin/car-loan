import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export function RangeCheck(valueMin: number, valueMax: number): ValidatorFn {

    return (control: AbstractControl): ValidationErrors | null => {

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: RangeCheck Validator :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::control::', control,
        //     '\n::control.value::', control.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( control.value < valueMin || control.value > valueMax ) {
            return {RangeCheck: true};
        }
        return null;
    }
}
