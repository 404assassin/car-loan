import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
// Components
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
import { DisqualificationComponent } from './components/pages/disqualification/disqualification.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { NewAccountComponent } from './components/pages/new-account/new-account.component';

const routes: Routes = [
    {path: '', redirectTo: 'landing', pathMatch: 'full'},
    {path: 'disqualification', component: DisqualificationComponent},
    {path: 'landing', component: LandingComponent},
    {path: 'new-account', component: NewAccountComponent},
    // {path: '**', component: 404Component}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
