import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStoreService } from '../../../services/data-store.service';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ConfirmMatchValidator } from '../../../validators/confirm-match.validator';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        // return !!(control && control.invalid && (control.dirty || control.touched));
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

interface LooseObject {
    [key: string]: any;
}

@Component({
    selector: 'app-new-account',
    templateUrl: './new-account.component.html',
    styleUrls: ['./new-account.component.scss']
})
export class NewAccountComponent implements AfterViewInit, OnInit {
    public serviceData: LooseObject;
    public loanQualifierData: LooseObject;

    public calculatorForm = new FormGroup({
            username: new FormControl('',
                [
                    Validators.required,
                    Validators.email,
                ]),
            password: new FormControl('',
                [
                    Validators.required,
                    Validators.minLength(8),
                    Validators.pattern(/(^.*(?=.*[a-zA-Z])(?=.*[!@#$%^&*_0-9]).*$)/),
                ]
            ),
            passwordConfirm: new FormControl('',
                [
                    Validators.required,
                ]
            ),
        },
        {
            validators: ConfirmMatchValidator
        }
    );

    matcher = new MyErrorStateMatcher();

    get validation() {
        return this.calculatorForm.controls;
    }

    constructor(/*<<<<::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>*/
                private dataStoreService: DataStoreService,
                private router: Router,
                /*<<<<::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>*/) {
        this.serviceData = this.dataStoreService.payloadSubject;
        this.loanQualifierData = this.dataStoreService.loanQualifierDataStore;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: NewAccountComponent constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::typeof this.serviceData.source.value !== \'undefined\' ::', typeof this.serviceData.source.value !== 'undefined',
        //     '\n::typeof this.loanQualifierData !== \'undefined\' ::', typeof this.loanQualifierData !== 'undefined',
        //     '\n::this.serviceData::', this.serviceData,
        //     '\n::this.loanQualifierData::', this.loanQualifierData,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    ngOnInit() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: NewAccountComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::typeof this.serviceData.source.value !== \'undefined\'::', typeof this.serviceData.source.value !== 'undefined',
        //     '\n::this.serviceData::', this.serviceData,
        //     '\n::this.loanQualifierData::', this.loanQualifierData,
        //     '\n::this.source.serviceData.value::', this.serviceData.source.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this.serviceData.source.value.qualified === 'undefined' ) {
            this.router.navigate(['/landing/']);
        } else {
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: NewAccountComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::this.serviceData::', this.serviceData,
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );
        }
    }

    ngAfterViewInit() {

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: NewAccountComponent ngAfterViewInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.serviceData::', this.serviceData,
        //     '\n::this.serviceData.value::', this.serviceData.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public submitUser(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: NewAccountComponent submitUser :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.calculatorForm.controls::', this.calculatorForm.controls,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public onChange(): void {

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: NewAccountComponent onChange :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.calculatorForm::', this.calculatorForm,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if(this.calculatorForm.errors !== null && this.calculatorForm.errors.confirmMatchValidator === true){
            this.calculatorForm.controls['passwordConfirm'].setErrors({'incorrect': true});
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: NewAccountComponent onChange :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::calculatorForm.errors?.confirmMatchValidator::', this.calculatorForm.errors.confirmMatchValidator,
            //     '\n::this.calculatorForm::', this.calculatorForm,
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );
        }
    }

}
