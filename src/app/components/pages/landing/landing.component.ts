import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoanQualifierService } from '../../../services/loan-qualifier.service';
import { DataStoreService } from '../../../services/data-store.service';
import { CurrencyPipe } from '@angular/common';
import { RangeCheck } from '../../../validators/range-check.validator';
import { Router } from '@angular/router';

interface LooseObject {
    [key: string]: any;
}

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

    public calculatorForm = new FormGroup({
            autoPurchasePrice: new FormControl('',
                [
                    Validators.required,
                ]),
            autoMake: new FormControl('',
                [
                    Validators.required,
                ]
            ),
            autoModel: new FormControl('',
                [
                    Validators.required,
                ]
            ),
            userEstimatedYearlyIncome: new FormControl('',
                [
                    Validators.required,
                ]
            ),
            userEstimatedCreditScore: new FormControl('',
                [
                    Validators.required,
                    RangeCheck(300, 850,),
                ]
            ),
        },
    );

    get validation() {
        return this.calculatorForm.controls;
    }

    constructor(/*<<<<::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>*/
                private currencyPipe: CurrencyPipe,
                private dataStoreService: DataStoreService,
                public loanQualifierService: LoanQualifierService,
                private router: Router,
                /*<<<<::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>*/) {
    }

    ngOnInit() {
    }

    public submitRequestCheck(): void {
        this.submitRequest();
    }

    private submitRequest(): void {
        const loanQualifierData: LooseObject = {
            autoPurchasePrice: parseFloat(this.calculatorForm.controls.autoPurchasePrice.value.replace(/[^0-9.-]+/g, '')),
            autoMake: this.calculatorForm.controls.autoMake.value,
            autoModel: parseFloat(this.calculatorForm.controls.autoModel.value),
            userEstimatedYearlyIncome: parseFloat(this.calculatorForm.controls.userEstimatedYearlyIncome.value.replace(/[^0-9.-]+/g, '')),
            userEstimatedCreditScore: parseFloat(this.calculatorForm.controls.userEstimatedCreditScore.value)
        };

        const loanQualifierDataStringify: string = JSON.stringify(loanQualifierData);
        this.quoteRequest(loanQualifierDataStringify);
    }

    private quoteRequest(loanQualifierData): void {
        this.loanQualifierService.postRequestDatas(loanQualifierData).subscribe(
            data => {
                // console.log(
                //     '\n::::::::::::::::::::::::::::::::::::::  quoteRequest  Post  :::::::::::::::::::::::::::::::::::::::::::::::::::',
                //     '\n::this::', this,
                //     '\n::POST Request is successful ::', data,
                //     '\n::data.name ::', data.name,
                //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                // );

                /**
                 * Possibly set a display element and update it with a successful message here.
                 * Could be staticly set here or part of the response.
                 **/

                // const statusDisplayObject: any = {
                //     'status': true
                // };

                // const success: any = {
                //     'success': {
                //         'message': '"' + data.name + '"' + ' successful'
                //     }
                // };


                this.responseUpdate(data);
                this.updateDataStore(data);
            },
            error => {
                console.error(
                    '\n::::::::::::::::::::::::::::::::::::::  quoteRequest  Post  Error  ::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::POST Request Error ::', error,
                    '\n::POST Request Error Message ::', error.message,
                    '\n::POST Request Error Message ::', error.error.message,
                    '\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            () => {
            }
        );
    }

    private responseUpdate(data: LooseObject): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: LandingComponent responseUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::data::', data,
        //     '\n::data.qualified::', data.qualified,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( !data.qualified ) {
            this.router.navigate(['/disqualification/']);
        } else {
            this.router.navigate(['/new-account/']);
        }
    }

    public updateDataStore(data: LooseObject): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: LandingComponent updateDataStore :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::data.qualified::', data.qualified,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.dataStoreService.setDataSet(data);
    }

    public currencyFormat($event: Event, formControlName: string): void {
        const tempValue = `${this.calculatorForm.value[formControlName]}`.replace(/\D/g, '');
        let valuePiped: any = 0;
        valuePiped = this.currencyPipe.transform(tempValue, 'USD', "symbol", '1.0-0');
        this.calculatorForm.patchValue({
            [formControlName]: valuePiped,
        });
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent currencyFormat :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::tempValue::', tempValue,
        //     '\n::this.calculatorForm::', this.calculatorForm,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

}
