import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { CurrencyPipe } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { LandingComponent } from './landing.component';
import { LoanQualifierService } from '../../../services/loan-qualifier.service';
import { By } from '@angular/platform-browser';

describe('LandingComponent', () => {
    let component: LandingComponent;
    let fixture: ComponentFixture<LandingComponent>;
    let userServiceStub: Partial<any>;
    let h1: HTMLElement;

    userServiceStub = {
        isLoggedIn: true,
        user: {name: 'Test User'}
    };
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LandingComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                MatFormFieldModule,
                MatInputModule,
                HttpClientModule,
                NoopAnimationsModule,
                RouterTestingModule.withRoutes([]),
            ],

            providers: [
                {provide: CurrencyPipe},
                {provide: LoanQualifierService, useValue: userServiceStub},
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LandingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        h1 = fixture.nativeElement.querySelector('h1');
    });

    it('Component should create', () => {
        expect(component).toBeTruthy();
    });

    it('Should display the landing page title', () => {
        expect(h1.textContent).toContain('Landing Page');
    });

    it('Calculator Form invalid when empty', () => {
        expect(component.calculatorForm.valid).toBeFalsy();
    });

    it('Auto Purchase Price field validity should be false when empty', () => {
        let formControl = component.calculatorForm.controls['autoPurchasePrice'];
        expect(formControl.valid).toBeFalsy();
    });
    it('Auto Purchase Price field validity should be required', () => {
        let errors = {};
        let formControl = component.calculatorForm.controls['autoPurchasePrice'];
        errors = formControl.errors || {};
        expect(errors['required']).toBeTruthy();
    });

    it('Auto Make field validity should be false when empty', () => {
        let formControl = component.calculatorForm.controls['autoMake'];
        expect(formControl.valid).toBeFalsy();
    });
    it('Auto Make field validity should be required', () => {
        let errors = {};
        let formControl = component.calculatorForm.controls['autoMake'];
        errors = formControl.errors || {};
        expect(errors['required']).toBeTruthy();
    });

    it('Auto Model field validity should be false when empty', () => {
        let formControl = component.calculatorForm.controls['autoModel'];
        expect(formControl.valid).toBeFalsy();
    });
    it('Auto Model field validity should be required', () => {
        let errors = {};
        let formControl = component.calculatorForm.controls['autoModel'];
        errors = formControl.errors || {};
        expect(errors['required']).toBeTruthy();
    });

    it('User Estimated Yearly Income field validity should be false when empty', () => {
        let formControl = component.calculatorForm.controls['userEstimatedYearlyIncome'];
        expect(formControl.valid).toBeFalsy();
    });
    it('User Estimated Yearly Income field validity should be required', () => {
        let errors = {};
        let formControl = component.calculatorForm.controls['userEstimatedYearlyIncome'];
        errors = formControl.errors || {};
        expect(errors['required']).toBeTruthy();
    });

    it('User Estimated Credit Score field validity should be false when empty', () => {
        let formControl = component.calculatorForm.controls['userEstimatedCreditScore'];
        expect(formControl.valid).toBeFalsy();
    });
    it('User Estimated Credit Score field validity should be required', () => {
        let errors = {};
        let formControl = component.calculatorForm.controls['userEstimatedCreditScore'];
        errors = formControl.errors || {};
        expect(errors['required']).toBeTruthy();
    });

/*    it('submitting a form emits a user', () => {
        expect(component.calculatorForm.valid).toBeFalsy();
        component.calculatorForm.controls['autoPurchasePrice'].setValue("15000");
        component.calculatorForm.controls['autoMake'].setValue("Dodge");
        component.calculatorForm.controls['autoModel'].setValue("Charger");
        component.calculatorForm.controls['userEstimatedYearlyIncome'].setValue(100000);
        component.calculatorForm.controls['userEstimatedCreditScore'].setValue(777);
        expect(component.calculatorForm.valid).toBeTruthy();
        let responseData: any;
        component.loanQualifierService.subscribe((value) => responseData = value);
        component.submitRequestCheck();
        expect(responseData.qualificationTitle).toBe("You qualify");
    });*/

});
