import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DisqualificationComponent } from './disqualification.component';

describe('DisqualificationComponent', () => {
    let component: DisqualificationComponent;
    let fixture: ComponentFixture<DisqualificationComponent>;
    beforeEach(async(() => {
        TestBed.configureTestingModule({

            imports: [
                HttpClientTestingModule,
                RouterTestingModule,
            ],
            declarations: [DisqualificationComponent],
            providers: [
                {
                    provide: Router,
                    useValue: {navigate: jasmine.createSpy('navigate')}
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DisqualificationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
