import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../services/data-store.service';
import { Router } from '@angular/router';

interface LooseObject {
    [key: string]: any;
}

@Component({
    selector: 'app-disqualification',
    templateUrl: './disqualification.component.html',
    styleUrls: ['./disqualification.component.scss']
})
export class DisqualificationComponent implements OnInit {
    public serviceData: LooseObject;
    public disqualificationData: LooseObject;

    constructor(/*<<<<::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>*/
                private dataStoreService: DataStoreService,
                private router: Router,
                /*<<<<::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>*/) {
        this.serviceData = this.dataStoreService.payloadSubject;
        this.disqualificationData = this.dataStoreService.loanQualifierDataStore;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: DisqualificationComponent constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::typeof this.serviceData.source.value !== \'undefined\' ::', typeof this.serviceData.source.value !== 'undefined',
        //     '\n::typeof this.disqualificationData !== \'undefined\' ::', typeof this.disqualificationData !== 'undefined',
        //     '\n::this.serviceData::', this.serviceData,
        //     '\n::this.disqualificationData::', this.disqualificationData,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    ngOnInit() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: DisqualificationComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::typeof this.serviceData.source.value !== \'undefined\'::', typeof this.serviceData.source.value !== 'undefined',
        //     '\n::this.serviceData::', this.serviceData,
        //     '\n::this.disqualificationData::', this.disqualificationData,
        //     '\n::this.source.serviceData.value::', this.serviceData.source.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this.serviceData.source.value.qualified === 'undefined' ) {
            this.router.navigate(['/landing']);
        } else {
            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: DisqualificationComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::this::', this,
                '\n::this.serviceData::', this.serviceData,
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );
        }
    }
}
