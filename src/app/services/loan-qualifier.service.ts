import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


interface LooseObject {
    [key: string]: any;
}

@Injectable({
    providedIn: 'root'
})
export class LoanQualifierService {

    constructor(/*<<<<::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>*/
                public httpClient: HttpClient
                /*<<<<::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>*/) {
    }

    public postRequestDatas(postData: any): Observable<any> {
        const postDataParsed: object = JSON.parse(postData);
        /**
         * if the purchase price is more than 1/5th of the income.
         **/
        const qualifierWage: boolean = postDataParsed['autoPurchasePrice'] < 1 * postDataParsed['userEstimatedYearlyIncome'] / 5;
        /**
         * If the estimated auto purchase price is below 600.
         **/
        const qualifierPurchasePrice: boolean = postDataParsed['autoPurchasePrice'] < 1000000;
        /**
         * If the submitted estimated credit score is below 600.
         **/
        const userEstimatedCreditScore: boolean = postDataParsed['userEstimatedCreditScore'] > 600;

        // console.log(
        //     '\n::::::::::::::::::::::::::::::::::::::  LoanQualifierService  postRequestDatas  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::postData::', postData,
        //     '\n::postDataParsed::', postDataParsed,
        //     '\n::qualifierWage::', qualifierWage,
        //     '\n::qualifierPurchasePrice::', qualifierPurchasePrice,
        //     '\n::userEstimatedCreditScore::', userEstimatedCreditScore,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

        if ( !qualifierWage ) {
            return this.getDisqualifiedDatas();
        } else if ( !qualifierPurchasePrice ) {
            return this.getDisqualifiedDatas();
        } else if ( !userEstimatedCreditScore ) {
            return this.getDisqualifiedDatas();
        } else {
            return this.getSuccessDatas();
        }

    }

    /**
     * Normally I'd include any logic around API service calls in the requesting component.
     * I've included it here as a simulation of the response logic taking place server side.
     **/
    private getSuccessDatas(): any {
        // console.log(
        //     '\n::::::::::::::::::::::::::::::::::::::  LoanQualifierService  getSuccessDatas  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return this.httpClient.get('assets/data/data-success.json', {responseType: 'json'});
    }

    private getDisqualifiedDatas(): any {
        // console.log(
        //     '\n::::::::::::::::::::::::::::::::::::::  LoanQualifierService  getDisqualifiedDatas  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return this.httpClient.get('assets/data/data-disqualified.json', {responseType: 'json'});
    }

}
