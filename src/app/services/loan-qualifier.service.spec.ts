import { async, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { LoanQualifierService } from './loan-qualifier.service';
import { LandingComponent } from '../components/pages/landing/landing.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RouterTestingModule } from '@angular/router/testing';
import { CurrencyPipe } from '@angular/common';

describe('LoanQualifierService', () => {


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LandingComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([]),
      ],

      providers: [
        {provide: CurrencyPipe},
      ]
    })
        .compileComponents();
  }));

    it('should be created', () => {
        const service: LoanQualifierService = TestBed.get(LoanQualifierService);
        expect(service).toBeTruthy();
    });
});
