import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs";

interface LooseObject {
    [key: string]: any
}

@Injectable({
    providedIn: 'root'
})
export class DataStoreService {
    public _payloadSubject: BehaviorSubject<LooseObject> = new BehaviorSubject<LooseObject>({});
    public readonly payloadSubject: Observable<any> = this._payloadSubject.asObservable();
    public loanQualifierDataStore: LooseObject = {};

    constructor() {
    }

    public setDataSet(payload: LooseObject) {
        this.loanQualifierDataStore = payload;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: DataStoreService setDataSet :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::payload::', payload,
        //     '\n::this._payloadSubject::', this._payloadSubject,
        //     '\n::this._payloadSubject.value::', this._payloadSubject.value,
        //     '\n::this.loanQualifierDataStore::', this.loanQualifierDataStore,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this._payloadSubject.next(payload);
    }

    public watchDataSet(): BehaviorSubject<LooseObject> {
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: DataStoreService BehaviorSubject watchDataSet :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this._payloadSubject::', this._payloadSubject,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        return this._payloadSubject;
    }

}
