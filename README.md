# CarLoanCalculator

## Build installing dependencies
After cloning the repo run  `npm install` from within the projects root. 

*As a note: I have recently noticed that one of the dependencies has been throwing an error if you have Python 3 install. If you are on a PC the dependency error can be ignored as the error only only effects Macs. Alternatively you can install Python 2 to squash this.*

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.0.
To install Angular CLI globally run `npm install -g @angular/cli`. Additional CLI documentation here [Angular CLI Commands](https://angular.io/cli).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
